
//! # MerMulIter
//! 
//! Provides a single Iterator adaptor that wraps an arbitrary number of
//! Iterators and merges their output, thus
//! _Mer_ ging _Mul_ tiple _Iter_ ators

use std::collections::BinaryHeap;
use std::cmp::Ordering;
use std::iter::Peekable;
use std::cmp::Reverse;

/// Proxy holds the sort key of the next item in an associated (Peekable)
/// Iterator.
///
/// The associated Iterator is identified by `iid`.
/// This is expected to be cheap to instantiate and copy to/within a
/// BinaryHeap.
///
/// TODO: It might be slightly faster for Proxy to hold a reference to the
/// Iterator for which it is a proxy, but this will surely entail lifetime
/// management headaches. Explore this later.
///
/// # Implementation notes
///
/// std::cmp::Reverse is necessary if the intended ordering of elements my
/// MerMulIter is ascending, which is conventional, because BinaryHeap is a
/// max-heap by default.
/// TODO: Should make this a parameter. Later!

struct Proxy<K> 
	where K : Ord {

	// The value by which Items in the the Iterators are ordered.
	key : Reverse<K>,
	// Integer ID, an offset into the MerMulIter::source : Vector.
	iid : usize,
}


impl<K> Ord for Proxy<K>
	where K : Ord {

	fn cmp(&self, other: &Self) -> Ordering {
		self.key.cmp(&other.key)
	}
}

impl<K> PartialOrd for Proxy<K> 
	where K : Ord {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		Some(self.cmp(other))
	}
}

impl<K> Eq for Proxy<K>
	where K : Ord { }

impl<K> PartialEq for Proxy<K>
	where K : Ord {
	fn eq( &self, other: &Self ) -> bool {
		self.key.eq(&other.key)
	}
}

////////////////////////////////////////////////////////////////////////////

/// This struct merge-sorts the content of an arbitrary number of Iterators.
/// * The wrapped Iterators must have identical Item types.
/// * The common Item type must yield a _key_ (to the closure _F_) that is Ord.
/// * Each iterator's content is _assumed_ to already be internally ordered,
///   so MerMulIter is simply interleaving the results of their next methods.
///   Currently no check is performed, though see below.
/// * Only ascending iteration is currently supported.
///
/// I intended to debug_assert! the well-orderedness of the wrapped
/// Iterators, but this seemed to require that the _K_ was also Copy.
/// TODO: There might be a way around this. Need to explore.

pub struct MerMulIter<I,K,F>
	where
		I:Iterator,
		K:Ord,
		F:Fn(&I::Item) -> K {
	/// Iterators being merged.
	source : Vec< Peekable<I> >,
	sorted : BinaryHeap< Proxy<K> >,
	key_fn : F
}


impl<I,K,F> MerMulIter<I,K,F>
	where
		I:Iterator,
		K:Ord,
		F:Fn(&I::Item) -> K {

	pub fn new( source : Vec<I>, key_fn : F ) -> MerMulIter<I,K,F> {
		let cap = source.len();
		let mut mmi = MerMulIter {
			source : Vec::with_capacity( cap ),
			sorted : BinaryHeap::with_capacity( cap ),
			key_fn
		};
		for s in source {
			mmi.source.push( s.peekable() );
		}
		for i in 0..mmi.source.len() {
			mmi.proxy( i );
		}
		mmi
	}

	/// Create and push into the priority queue a Proxy for the Iterator
	/// at offset iid.

	fn proxy( &mut self, iid : usize ) /*-> Option<K>*/ {
		if let Some(item) = self.source[ iid ].peek() {
			let key = (self.key_fn)(item);
			self.sorted.push( Proxy { key : Reverse::<K>( key ), iid } );
			//Some(key)
		}/*else {
			None
		}*/
	}
}


impl<I,K,F> Iterator for MerMulIter<I,K,F> 
	where
		I:Iterator,
		K:Ord,
		F:Fn(&I::Item) -> K {

	type Item = (usize,I::Item);

	fn next( &mut self ) -> Option<Self::Item> {

		if let Some(top) = self.sorted.pop() {

			let s = &mut self.source[ top.iid ];
			let val = s.next().unwrap(); // safe because in sorted
			/*let nxt =*/ self.proxy( top.iid );
			//debug_assert!( nxt.map_or( true, |n| (self.key_fn)(&val) <= n ) );
			Some( ( top.iid, val ) )

		} else {

			None
		}
	}
}


#[cfg(test)]
mod tests {
	use crate::*;
	#[test]
	fn it_works() {
		let i1 = vec![ 0, 2, 6, 7, 10 ].into_iter();
		let i2 = vec![ 1, 3, 4, 5, 8, 9 ].into_iter();
		let m = MerMulIter::new( vec![i1,i2], |s| *s );
		for (i,(src,item)) in m.enumerate() {
			assert_eq!(i,item);
			println!("{}:{}", src, item );
		}
	}
}

